<?php

namespace Drupal\update_exclude\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UpdateExcludeSettingsForm. The config form for the update_exclude module.
 *
 * @package Drupal\update_exclude\Form
 */
class UpdateExcludeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'update_exclude.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'update_exclude_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['excluded-projects'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Projects to exclude'),
      '#description' => $this->t('Add one project (machine name) per line'),
      '#default_value' => $this->config('update_exclude.settings')
        ->get('excluded_projects'),
    ];

    update_refresh();
    /** @var \Drupal\update\UpdateManager $update_manager*/
    $update_manager = \Drupal::service('update.manager');
    $projects = $update_manager->getProjects();
    $header = [t('Project'), t('Project name')];
    $rows = [];
    foreach ($projects as $key => $value) {
      $rows[] = ['project' => $key, 'name' => $value['info']['name']];
    }
    $form['not-excluded'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('The following will be not excluded'),
    ];
    $form['not-excluded']['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('update_exclude.settings')
      ->set('excluded_projects', $form_state->getValue('excluded-projects'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
